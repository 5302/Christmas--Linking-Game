package com.cn.scnu.likgame.service;

import com.cn.scnu.likgame.dao.GameDao;

public class Gamejudge {

	private GameDao gamedao;
	
	public  Gamejudge(GameDao gamedao){
		
		this.gamedao = gamedao;
	}
	
	public void map_init(){
			gamedao.gamemap = new int[GameDao.MAPW][GameDao.MAPH];
			for(int x=0;x<GameDao.MAPW;x++)
				for(int y=0;y<GameDao.MAPH;y++)
					gamedao.gamemap[x][y]=GameDao.static_map[y+(gamedao.level-1)*GameDao.MAPH][x];
	}

	public boolean isblank(){
		for(int x=0;x<GameDao.MAPW;x++)
			for(int y=0;y<GameDao.MAPH;y++)
				if(gamedao.gamemap[x][y]!=0)
					return false;
		return true;
	}
	
	public boolean cancel(int nowx,int nowy,int nextx,int nexty){
		
		if(zeroturn(nowx,nowy,nextx,nexty) || oneturn(nowx,nowy,nextx,nexty) || twoturn(nowx,nowy,nextx,nexty)) 
			return true;
		return false;
	}
	
	private boolean same_xline(int x1,int y1,int x2,int y2){
		if(x1!=x2)
			return false;
		int min=Math.min(y1, y2);
		int max=Math.max(y1, y2);
		for(int i = min+1;i<max;i++){
			if(gamedao.gamemap[x1][i]!=0)
				return false;
		}
		return true;
	}

	private boolean same_yline(int x1,int y1,int x2,int y2){
		if(y1!=y2)
			return false;
		int min=Math.min(x1,x2);
		int max=Math.max(x1, x2);
		for(int i=min+1;i<max;i++){
			if(gamedao.gamemap[i][y1]!=0)
				return false;
		}
		return true;
	}
	
	private boolean zeroturn(int nowx,int nowy,int nextx,int nexty){
		
		if(same_xline(nowx,nowy,nextx,nexty) || same_yline(nowx,nowy,nextx,nexty)){
			
			gamedao.p[0].x=nowx;gamedao.p[0].y=nowy;
			gamedao.p[1].x=nowx;gamedao.p[1].y=nowy;
			gamedao.p[2].x=nowx;gamedao.p[2].y=nowy;
			gamedao.p[3].x=nextx;gamedao.p[3].y=nexty;

			return true;
		};
		return false;
	}
	
	private boolean oneturn(int nowx,int nowy,int nextx,int nexty){
		
		if(gamedao.gamemap[nowx][nexty]==0 && same_xline(nowx,nowy,nowx,nexty) && same_yline(nowx,nexty,nextx,nexty) ){
			
			gamedao.p[0].x=nowx;gamedao.p[0].y=nowy;
			gamedao.p[1].x=nowx;gamedao.p[1].y=nexty;
			gamedao.p[2].x=nowx;gamedao.p[2].y=nexty;
			gamedao.p[3].x=nextx;gamedao.p[3].y=nexty;				
			return true;
		}

		if(gamedao.gamemap[nextx][nowy]==0 && same_xline(nextx,nowy,nextx,nexty) && same_yline(nowx,nowy,nextx,nowy)){
			
			gamedao.p[0].x=nowx;gamedao.p[0].y=nowy;
			gamedao.p[1].x=nextx;gamedao.p[1].y=nowy;
			gamedao.p[2].x=nextx;gamedao.p[2].y=nowy;
			gamedao.p[3].x=nextx;gamedao.p[3].y=nexty;
			return true;
		}
		return false;
	}
	
	private boolean twoturn(int nowx,int nowy,int nextx,int nexty){
		
		if(upsearch(nowx,nowy,nextx,nexty) || 
		   downsearch(nowx,nowy,nextx,nexty) ||
		   leftsearch(nowx,nowy,nextx,nexty) ||
		   rightsearch(nowx,nowy,nextx,nexty)  
		){
			return true;
		}
		return false;
	}
	
	private boolean upsearch(int x0,int y0,int x,int y){
		
		for(int i=y0-1;;i--){
			if(i<0 || gamedao.gamemap[x0][i]!=0)
				break;
			if(same_xline(x0,y0,x0,i) && same_yline(x0,i,x,i) && same_xline(x,i,x,y) &&
					gamedao.gamemap[x][i]==0 && gamedao.gamemap[x0][i]==0){
				
				gamedao.p[0].x=x0;gamedao.p[0].y=y0;
				gamedao.p[1].x=x0;gamedao.p[1].y=i;
				gamedao.p[2].x=x;gamedao.p[2].y=i;
				gamedao.p[3].x=x;gamedao.p[3].y=y;
				return true;
			}
		}
		return false;
		
	}
	
	private boolean downsearch(int x0,int y0,int x,int y){
		
		for(int i=y0+1;;i++){
			if(i>=GameDao.MAPH || gamedao.gamemap[x0][i]!=0)
				break;
			if(same_xline(x0,y0,x0,i) && same_yline(x0,i,x,i) && same_xline(x,i,x,y) &&
					gamedao.gamemap[x][i]==0 && gamedao.gamemap[x0][i]==0){
				gamedao.p[0].x=x0;gamedao.p[0].y=y0;
				gamedao.p[1].x=x0;gamedao.p[1].y=i;
				gamedao.p[2].x=x;gamedao.p[2].y=i;
				gamedao.p[3].x=x;gamedao.p[3].y=y;
				return true;
			}
		}
		return false;
		
	}
	
	private boolean leftsearch(int x0,int y0,int x,int y){
		
		for(int i=x0-1;;i--){
			if(i<0 || gamedao.gamemap[i][y0]!=0)
				break;
			if(same_yline(x0,y0,i,y0) && same_xline(i,y0,i,y) && same_yline(i,y,x,y) &&
					gamedao.gamemap[i][y0]==0 && gamedao.gamemap[i][y]==0){
				gamedao.p[0].x=x0;gamedao.p[0].y=y0;
				gamedao.p[1].x=i;gamedao.p[1].y=y0;
				gamedao.p[2].x=i;gamedao.p[2].y=y;
				gamedao.p[3].x=x;gamedao.p[3].y=y;
				return true;
			}
		}
		return false;		
	}
	
	private boolean rightsearch(int x0,int y0,int x,int y){
		
		for(int i=x0+1;;i++){
			if(i>=GameDao.MAPW  || gamedao.gamemap[i][y0]!=0)
				break;
			if(same_yline(x0,y0,i,y0) && same_xline(i,y0,i,y) && same_yline(i,y,x,y) && 
					gamedao.gamemap[i][y0]==0 && gamedao.gamemap[i][y]==0){
				gamedao.p[0].x=x0;gamedao.p[0].y=y0;
				gamedao.p[1].x=i;gamedao.p[1].y=y0;
				gamedao.p[2].x=i;gamedao.p[2].y=y;
				gamedao.p[3].x=x;gamedao.p[3].y=y;
				return true;
			}
		}
		return false;		
	}
	

}
