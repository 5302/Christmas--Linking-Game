package com.cn.scnu.likgame.dao;

import java.awt.Graphics;
import java.awt.Point;

import com.cn.scnu.likgame.entity.ButtonWin;
import com.cn.scnu.likgame.entity.ChooseWin;
import com.cn.scnu.likgame.entity.Pause;
import com.cn.scnu.likgame.entity.Rect;
import com.cn.scnu.likgame.entity.TimerWin;
import com.cn.scnu.likgame.service.Gamejudge;

public class GameDao {

	public int[][] gamemap;
	
	public static final int MAPW =  10;
	
	public static final int MAPH = 6;
	
	private Rect rect;
	
	public Gamejudge gamejudge =new Gamejudge(this);
	
	public ChooseWin choosewin;
	
	public ButtonWin button = new ButtonWin(5,10);
	
	public ChooseWin choose = new ChooseWin(-10,-10);

	public Pause pause = new Pause(32,495);
	
	public TimerWin timerwin = new TimerWin(1,480);
	
	public int mark=0;
	
	public int level=1;
	
	public int settime = 59;
	
	public int time = settime ;
	
	public boolean over = false;
	
	public boolean won = false;
	
	public Point []p = new Point[4];

	public static final int static_map[][]={{4,0,1,0,3,4,0,1,0,3},
											   {0,2,0,2,0,0,2,0,2,0},
											   {3,0,1,0,4,3,0,1,0,4},
											   {4,0,1,0,3,4,0,1,0,3},
											   {0,2,0,2,0,0,2,0,2,0},
											   {3,0,1,0,4,3,0,1,0,4},
											   
											   {8,7,8,8,7,8,7,8,8,7},
											   {0,8,0,4,0,0,8,0,4,0},
											   {7,6,4,6,7,7,6,4,6,7},
											   {8,7,8,8,7,8,7,8,8,7},
											   {0,8,0,4,0,0,8,0,4,0},
											   {7,6,4,6,7,7,6,4,6,7},
											   
											   {13,5,12,5,10,13,5,12,5,10},
											   {9,1,0,11,10,9,1,0,11,10},
											   {12,13,9,11,1,12,13,9,11,1},
											   {13,5,12,5,10,13,5,12,5,10},
											   {9,1,0,11,10,9,1,0,11,10},
											   {12,13,9,11,1,12,13,9,11,1}
											   };											
	
	public GameDao(){
		
		for(int i=0;i<4;i++){
			p[i]=new Point();
			p[i].x=-1;
			p[i].y=-1;
		}
		
		gamejudge.map_init();
	}
	
	public void drawmap(Graphics g){
		
		for(int x=0;x<MAPW ;x++)
			for(int y=0;y<MAPH ;y++){
				rect =	new Rect(gamemap[x][y],x,y);
				rect.draw(g);
			}
		
	}
}
