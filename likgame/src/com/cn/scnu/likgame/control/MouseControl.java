package com.cn.scnu.likgame.control;

import java.awt.Cursor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MouseControl extends MouseAdapter {

	private GameControl gamecontrol;
	
	public MouseControl(GameControl gamecontrol){
		
		this.gamecontrol = gamecontrol;
		
	}
	
	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
		/*
		 * 游戏区域
		 */
		if(e.getX()>170 && e.getY()>140 && e.getX()<170+126*5 && e.getY()<140+126*3 && 
				!gamecontrol.gamedao.over && !gamecontrol.gamedao.pause.pause){
			
			int nowx = (e.getX()-170)/63;							//转化为地图坐标
			int nowy = (e.getY()-140)/63;							//转化为地图坐标
			
			if(gamecontrol.gamedao.choose.x>=0 && 												//区域内
					!(gamecontrol.gamedao.choose.x==nowx && gamecontrol.gamedao.choose.y==nowy) &&			//不是同个位置
					gamecontrol.gamedao.gamemap[gamecontrol.gamedao.choose.x][gamecontrol.gamedao.choose.y]==gamecontrol.gamedao.gamemap[nowx][nowy] &&	//是同种方块
					gamecontrol.gamedao. gamejudge.cancel(gamecontrol.gamedao.choose.x, gamecontrol.gamedao.choose.y, nowx, nowy)			//是否可消
			){
				gamecontrol.gamedao.gamemap[gamecontrol.gamedao.choose.x][gamecontrol.gamedao.choose.y]=0;	
				gamecontrol.gamedao.gamemap[nowx][nowy]=0;	
				
				gamecontrol.burn1.x=gamecontrol.gamedao.choose.x;
				gamecontrol.burn1.y=gamecontrol.gamedao.choose.y;
				gamecontrol.burn2.x=nowx;
				gamecontrol.burn2.y=nowy;
				gamecontrol.gametimer.startburn();			
				
				gamecontrol.gamedao.choose.x=-10;
				gamecontrol.gamedao.choose.y=-10;

			}
			else if(gamecontrol.gamedao.gamemap[nowx][nowy]!=0){
				gamecontrol.gamedao.choose.x=nowx;
				gamecontrol.gamedao.choose.y=nowy;
			}
			else{																		//
				gamecontrol.gamedao.choose.x=-10;
				gamecontrol.gamedao.choose.y=-10;				
			}
			gamecontrol.panel.repaint();
		}
		
		/*
		 * 按键区域
		 */
		if(e.getX()>5 && e.getY()>10 && e.getX()<5+90  && e.getY()<10+110*4){
			gamecontrol.gamedao.button.choose=(e.getY()-10)/110;
			if(gamecontrol.gamedao.button.choose<3){
				gamecontrol.gamedao.level=gamecontrol.gamedao.button.choose+1;
				gamecontrol.gamedao.gamejudge.map_init();
				gamecontrol.gamedao.pause.pause=true;
				gamecontrol.gamedao.time=gamecontrol.gamedao.settime;
			}
			gamecontrol.panel.repaint();
		}
		
		/*
		 * 时间轴区域
		 */
		if(e.getX()>30 && e.getY()>500 && e.getX()<85  && e.getY()<550){
			gamecontrol.gamedao.pause.pause=!gamecontrol.gamedao.pause.pause;
			if(gamecontrol.gamedao.pause.pause)
				gamecontrol.gamedao.won=false;
			gamecontrol.panel.repaint();
		}
	}
	
    @Override
    public void mouseMoved(MouseEvent e) {
    	if((e.getX()>30 && e.getY()>500 && e.getX()<85  && e.getY()<550) ||
    		(e.getX()>5 && e.getY()>10 && e.getX()<5+90  && e.getY()<10+110*4)){
    		gamecontrol.frame.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    	}
    	else{
    		gamecontrol.frame.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
    	}
    }
	
}
