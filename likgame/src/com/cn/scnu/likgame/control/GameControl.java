package com.cn.scnu.likgame.control;

import com.cn.scnu.likgame.dao.GameDao;
import com.cn.scnu.likgame.entity.Burning;
import com.cn.scnu.likgame.entity.Wait;
import com.cn.scnu.likgame.entity.Won;
import com.cn.scnu.likgame.ui.JFrameGame;
import com.cn.scnu.likgame.ui.JPanelGame;




public class GameControl {

	public JPanelGame panel;
	
	public JFrameGame frame ;

	public GameDao gamedao;
	
	public TimerControl gametimer;
	
	public Burning burn1 = new Burning(-10,-10);
	
	public Burning burn2 = new Burning(-10,-10);
	
	public Won won = new Won(270,200); 
	
	public Wait wait = new Wait(360,240);
	
	//public BloodWin blood = new BloodWin(115,40);

	public GameControl() {
	
		// 创建游戏面板
		this.panel = new JPanelGame(this);
		
		// 创建游戏窗口
		frame = new JFrameGame(panel);
		frame.setVisible(true);
		
		//添加鼠标事件
		MouseControl mouse = new MouseControl(this);
		frame.addMouseListener(mouse);
		frame.addMouseMotionListener(mouse);
		
		
		gamedao = new GameDao();
		gametimer= new TimerControl(this);
		gametimer.bloodtimer.start();
		//gametimer.wontimer.start();				//等待胜利

	}



	
	
}
