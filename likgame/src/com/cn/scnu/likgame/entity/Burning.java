package com.cn.scnu.likgame.entity;

import java.awt.Graphics;

public class Burning {
	
	protected static final int RECTW = 63;
	
	protected static final int RECTH = 63; 
	
	public int x ;
	
	public int y ;
	
	public int frame = 0;
	
	public GameIcons icons = new GameIcons();
	
	public Burning(int x,int y){
		
		this.x=x;
		
		this.y=y;
		
	}
	
	public void draw(Graphics g){
		
		g.drawImage(icons.BURN_IMG[frame],170+x*RECTW, 140+y*RECTH, null);
		
	}
}
