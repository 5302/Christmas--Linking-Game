package com.cn.scnu.likgame.entity;

import java.awt.Graphics;

public class Pause {
	
	protected static final int RECTW = 65;
	
	protected static final int RECTH = 65; 
	
	public int x ;
	
	public int y ;
	
	public boolean pause=true;
	
	public GameIcons icons = new GameIcons();
	
	public Pause(int x,int y){
		
		this.x=x;
		
		this.y=y;
		
	}
	
	public void draw(Graphics g){
		
		if(pause)
			g.drawImage(icons.PAUSE_IMG, x, y, null);
		
	}	
}
