package com.cn.scnu.likgame.entity;

import java.awt.Image;

import javax.swing.ImageIcon;

public class GameIcons {
	
	public Image[] RECT_IMG = new Image[14];
	
	public Image[] BUTTONWIN_IMG = new Image[4];

	public Image[] BUTTONWINC_IMG = new Image[4];
	
	public Image[] BURN_IMG = new Image[9];

	public Image[] WON_IMG = new Image[4];
	
	public Image TIMER_IMG ;
	
	public Image CHOOSE_IMG;
	
	public Image PAUSE_IMG;
	
	public Image BLOOD_IMG;

	public Image WAIT_IMG;
	
	public GameIcons(){
	
		for(int i=0;i<4;i++){
			BUTTONWIN_IMG[i] = new ImageIcon("image/button/btn"+(i+1)+".png").getImage();
		}

		for(int i=0;i<4;i++){
			BUTTONWINC_IMG[i] = new ImageIcon("image/button/btnc"+(i+1)+".png").getImage();
		}
		
		for(int i=0;i<14;i++){
			RECT_IMG[i] = new ImageIcon("image/rect/"+i+".png").getImage();
		}

		for(int i=0;i<9;i++){
			BURN_IMG[i] = new ImageIcon("image/burning/burn"+i+".png").getImage();
		}
		
		for(int i=0;i<4;i++){
			WON_IMG[i] = new ImageIcon("image/won/won"+i+".png").getImage();
		}
		
		WAIT_IMG= new ImageIcon("image/win/wait.png").getImage();
		TIMER_IMG= new ImageIcon("image/timer/time.png").getImage();
		CHOOSE_IMG = new ImageIcon("image/win/choose.png").getImage();
		BLOOD_IMG = new ImageIcon("image/win/blood.png").getImage();
		PAUSE_IMG = new ImageIcon("image/button/pause.png").getImage();
	}

}
