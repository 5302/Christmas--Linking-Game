package com.cn.scnu.likgame.entity;

import java.awt.Graphics;


public class Rect {

	protected static final int RECTW = 63;
	
	protected static final int RECTH = 63; 
	
	public int x ;
	
	public int y ;
	
	public int num;
	
	public GameIcons icons = new GameIcons();
	
	public Rect(int num,int x,int y){
		
		this.num=num;
		
		this.x=x;
		
		this.y=y;
		
	}
	
	public void draw(Graphics g){
		
		g.drawImage(icons.RECT_IMG[num], 170+x*RECTW, 140+y*RECTH, null);
		
	}
}
	
		

