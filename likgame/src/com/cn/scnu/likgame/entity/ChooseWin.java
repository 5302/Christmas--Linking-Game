package com.cn.scnu.likgame.entity;

import java.awt.Graphics;

public class ChooseWin {
	
	protected static final int RECTW = 63;
	
	protected static final int RECTH = 63; 
	
	public int x ;
	
	public int y ;
	
	public GameIcons icons = new GameIcons();
	
	public ChooseWin(int x,int y){
		
		this.x=x;
		
		this.y=y;
		
	}
	
	public void draw(Graphics g){
		
		g.drawImage(icons.CHOOSE_IMG, 170+x*RECTW, 140+y*RECTH, null);
		
	}	
}
