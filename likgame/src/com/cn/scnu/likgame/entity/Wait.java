package com.cn.scnu.likgame.entity;

import java.awt.Graphics;

public class Wait {
	
	protected static final int RECTW = 130;
	
	protected static final int RECTH = 112; 
	
	public int x ;
	
	public int y ;
	
	public GameIcons icons = new GameIcons();
	
	public Wait(int x,int y){
		
		this.x=x;
		
		this.y=y;
		
	}
	
	public void draw(Graphics g){
		
		g.drawImage(icons.WAIT_IMG,x, y, null);
		
	}
}
