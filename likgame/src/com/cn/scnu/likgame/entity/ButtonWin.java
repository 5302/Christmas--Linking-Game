package com.cn.scnu.likgame.entity;

import java.awt.Graphics;

public class ButtonWin {
	
	protected static final int RECTW = 90;
	
	protected static final int RECTH = 110; 
	
	public int x ;
	
	public int y ;
	
	public int choose=0;
	
	public GameIcons icons = new GameIcons();
	
	public ButtonWin(int x,int y){
		
		this.x=x;
		
		this.y=y;
		
	}

	public void draw(Graphics g){
		
		for(int i =0 ; i<4; i++){
			if(choose == i)
				g.drawImage(icons.BUTTONWINC_IMG[i], x, y+i*RECTH, null);
			else
				g.drawImage(icons.BUTTONWIN_IMG[i], x, y+i*RECTH, null);
		}
			
		
	}	

}
