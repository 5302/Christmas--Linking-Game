package com.cn.scnu.likgame.ui;

import java.awt.BorderLayout;

import javax.swing.JFrame;

public class JFrameGame extends JFrame {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7629159316693020768L;

	public JFrameGame(JPanelGame panel){
		//设置标题
		this.setTitle("连连看");
		//设置布局
		this.setLayout(new BorderLayout());
		//设置窗体大小
		this.setSize(840,600);
		//居中
		this.setLocationRelativeTo(null);
		//关闭事件
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setUndecorated(true);
		
		//设置默认Panel
		this.setContentPane(panel);
	}
}