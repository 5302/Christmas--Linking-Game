package com.cn.scnu.likgame.ui;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.ImageIcon;

public class Background {
	
	private Image background = new ImageIcon("image/background/likback.png").getImage();
	
	public void draw(Graphics g){
		g.drawImage(background, 0, 0, null);
	}
}
