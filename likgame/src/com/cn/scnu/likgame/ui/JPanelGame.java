package com.cn.scnu.likgame.ui;


import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.JPanel;

import com.cn.scnu.likgame.control.GameControl;


@SuppressWarnings("serial")
public class JPanelGame extends JPanel{
	
	private final static Background back = new Background();
	
	private GameControl gamecontrol;
	
	public JPanelGame(GameControl gamecontrol){
		
		this.gamecontrol = gamecontrol;
		
	}
	
	@Override
	public void paint(Graphics g){
	
		
		back.draw(g);
		
		g.setColor( Color.red);
		for(int i =0;i<3;i++){
			g.drawLine(168+63/2+63*gamecontrol.gamedao.p[i].x, 140+63/2+63*gamecontrol.gamedao.p[i].y,168+63/2+63*gamecontrol.gamedao.p[i+1].x, 140+63/2+63*gamecontrol.gamedao.p[i+1].y);
		}
	
		gamecontrol.gamedao.drawmap(g);
		gamecontrol.gamedao.choose.draw(g);
		gamecontrol.gamedao.button.draw(g);
		gamecontrol.gamedao.timerwin.draw(g);
		gamecontrol.gamedao.pause.draw(g);
		
		g.setFont(new Font("����",Font.BOLD,18));
		g.setColor( Color.WHITE);
		if(!gamecontrol.gamedao.pause.pause)
			if(gamecontrol.gamedao.time>9)
				g.drawString("00:"+gamecontrol.gamedao.time, 39, 535);
			else
				g.drawString("00:0"+gamecontrol.gamedao.time, 39, 535);
		else
			gamecontrol.wait.draw(g);
		//gamecontrol.gamedao.bloodwin.draw(g);
		
		gamecontrol.burn1.draw(g);
		gamecontrol.burn2.draw(g);	
		gamecontrol.won.draw(g);
		
	}
}
